<?php

use Illuminate\Support\Facades\Route;
use Andrlee\PageBuilder\PageBuilder;

// handle pagebuilder asset requests
Route::any( config('pagebuilder.general.assets_url') . '{any}', function() {

    $builder = new PageBuilder(config('pagebuilder'));
    $builder->handlePageBuilderAssetRequest();

})->where('any', '.*');


// handle requests to retrieve uploaded file
Route::any( config('pagebuilder.general.uploads_url') . '{any}', function() {

    $builder = new PageBuilder(config('pagebuilder'));
    $builder->handleUploadedFileRequest();

})->where('any', '.*');


if (config('pagebuilder.website_manager.use_website_manager')) {

    // handle all website manager requests
    Route::any( config('pagebuilder.website_manager.url') . '{any}', function() {
        echo 'sadasdasdsadasd';
        die();

        $builder = new PageBuilder(config('pagebuilder'));
        $builder->handleRequest();

    })->where('any', '.*');

}


if (config('pagebuilder.router.use_router')) {

    // pass all remaining requests to the PageBuilder router
    Route::any( '/{any}', function() {

        $builder = new PageBuilder(config('pagebuilder'));
        $hasPageReturned = $builder->handlePublicRequest();

        if (request()->path() === '/' && ! $hasPageReturned) {
            $builder->getWebsiteManager()->renderWelcomePage();
        }

    })->where('any', '.*');

}
