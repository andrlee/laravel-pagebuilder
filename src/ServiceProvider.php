<?php

namespace Andrlee\PageBuilder;

use Andrlee\PageBuilder\Commands\CreateTheme;
use Andrlee\PageBuilder\Commands\PublishDemo;
use Andrlee\PageBuilder\Commands\PublishTheme;
use Andrlee\PageBuilder\PageBuilder;
use Exception;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
    }

    /**
     * Bootstrap services.
     *
     * @return void
     * @throws Exception
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
        $this->loadMigrationsFrom(__DIR__ . '/../migrations');

        if ($this->app->runningInConsole()) {
            $this->commands([
                CreateTheme::class,
                PublishTheme::class,
                PublishDemo::class,
            ]);
        } elseif (empty(config('pagebuilder'))) {
            throw new Exception("No PageBuilder config found, please run: php artisan vendor:publish --provider=\"Andrlee\PageBuilder\ServiceProvider\" --tag=config");
        }

        // register singleton PageBuilder (this ensures phpb_ helpers have the right config without first manually creating a PageBuilder instance)
        $this->app->singleton('PageBuilder', function($app) {
            return new PageBuilder(config('pagebuilder') ?? []);
        });
        $this->app->make('PageBuilder');

        $this->publishes([
            __DIR__ . '/../config/pagebuilder.php' => config_path('pagebuilder.php'),
        ], 'config');
        $this->publishes([
            __DIR__ . '/../themes/demo' => base_path(config('pagebuilder.theme.folder_url') . '/demo'),
        ], 'demo-theme');
    }
}
