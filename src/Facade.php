<?php

namespace Andrlee\PageBuilder;

/**
 * @see \PageBuilder\PageBuilder
 */
class Facade extends \Illuminate\Support\Facades\Facade
{
    /**
     * {@inheritDoc}
     */
    protected static function getFacadeAccessor()
    {
        return PageBuilder::class;
    }
}
